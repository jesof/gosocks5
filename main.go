package main

func main() {
	Debug = true
	s, err := NewClassicServer("127.0.0.1:8888", "127.0.0.1", "", "", 0, 0, 0, 60)
	if err != nil {
		panic(err)
	}
	if err := s.Run(nil); err != nil {
		panic(err)
	}
}
